import torch.nn as nn
import torchvision
import numpy as np
from tqdm import tqdm
import time
import copy
import os
import torch
from torch.utils.tensorboard import SummaryWriter
from torchvision import datasets, models, transforms
from pytorchtools import *

writer = SummaryWriter()

num_epochs = 50
batch_size = 16
LEARNING_RATE = 0.001
patience = 20


def npy_loader(path):
    sample = torch.from_numpy(np.load(path, allow_pickle=True))
    return sample


def train_model(model, criterion, optimizer, scheduler, num_epochs):

    best_acc = 0.0
    best_epoch = 0

    since = time.time()
    best_model_wts = copy.deepcopy(model.state_dict())

    if not os.path.exists('/home/ubuntu/AM_Talking_v1/scripts/train/checkpoints'):
        os.makedirs("/home/ubuntu/AM_Talking_v1/scripts/train/checkpoints/exp0")
        save_path = "/home/ubuntu/AM_Talking_v1/scripts/train/checkpoints/exp0"
    else:
        exp_num = []
        for exp_folders in os.listdir('/home/ubuntu/AM_Talking_v1/scripts/train/checkpoints'):
            exp_num.append(int(exp_folders.split("exp")[1]))
        last_exp = max(exp_num)
        os.mkdir(f'/home/ubuntu/AM_Talking_v1/scripts/train/checkpoints/exp{last_exp + 1}')
        save_path = f'/home/ubuntu/AM_Talking_v1/scripts/train/checkpoints/exp{last_exp + 1}/'

    results_txt_path = os.path.join(save_path,"results.txt")

    with open(results_txt_path, "w") as f:
        f.write("epoch val_loss val_acc\n")

    early_stopping = EarlyStopping(patience=patience, verbose=True)

    early_stopping_flag = False

    for epoch in range(num_epochs):

        print('Epoch {}/{}'.format(epoch, num_epochs - 1))
        print('-' * 10)

        # Each epoch has a training and validation phase

        for phase in ['train', 'val']:

            if phase == 'train':
                model.train()  # Set model to training mode
                dl = train_dl

            else:
                dl = val_dl
                model.eval()   # Set model to evaluate mode

            running_loss = 0.0
            running_corrects = 0

            # Iterate over data.

            for iidx, d in enumerate(tqdm(dl)):
                # d = next(iter(dl))
                inputs, labels = d

                inputs = inputs.to(device)
                labels = labels.to(device)

                # zero the parameter gradients
                optimizer.zero_grad()

                # forward
                # track history if only in train
                with torch.set_grad_enabled(phase == 'train'):

                    outputs = model(inputs)
                    _, preds = torch.max(outputs, 1)
                    loss = criterion(outputs, labels)

                    # backward + optimize only if in training phase
                    if phase == 'train':
                        loss.backward()
                        optimizer.step()

                # statistics
                running_loss += loss.item() * inputs.size(0)
                running_corrects += torch.sum(preds == labels.data)
            #Ends of for loop

            epoch_loss = running_loss / dataset_sizes[phase]
            epoch_acc = running_corrects.double() / dataset_sizes[phase]

            if phase == 'train':
                scheduler.step(epoch_loss)
                writer.add_scalar("Loss/train", epoch_loss, epoch)
                writer.add_scalar("Accuracy/train", epoch_acc, epoch)

            if phase == 'val':
                writer.add_scalar("Loss/val", epoch_loss, epoch)
                writer.add_scalar("Accuracy/val", epoch_acc, epoch)

            print('{} Loss: {:.8f} Acc: {:.8}'.format(
                phase, epoch_loss, epoch_acc))

            # deep copy the model
            if phase == 'val':
                if epoch_acc > best_acc:
                    best_epoch = epoch
                    best_acc = epoch_acc
                print(f"VAL: best epoch is: {best_epoch} with accuracy: {best_acc}")

                torch.save(model,
                           save_path + f"e{epoch}_i{iidx}.pth")
                best_model_wts = copy.deepcopy(model.state_dict())
                with open(results_txt_path, "+a") as f:
                    f.write(str(epoch) + " " + str(epoch_loss) + " " + str(epoch_acc.to("cpu").numpy())+"\n")

                    early_stopping(epoch_loss, model)

                    if early_stopping.early_stop:
                        print("Early stopping")
                        early_stopping_flag = True
                        break

        if early_stopping_flag is True:
            break


    time_elapsed = time.time() - since
    print('Training complete in {:.0f}m {:.0f}s'.format(
        time_elapsed // 60, time_elapsed % 60))
    print(f'results saved at {save_path}')

    # load best model weights
    model.load_state_dict(best_model_wts)
    return model


# Create the model and put it on the GPU if available


device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
model = torchvision.models.resnet18(pretrained=True)
num_ftrs = model.fc.in_features
model.fc = nn.Linear(num_ftrs, 2)
model_conv = model.to(device)

train_path = "/home/ubuntu/AM_Talking_v1/data/oversampled_25sec/train"
val_path = "/home/ubuntu/AM_Talking_v1/data/oversampled_25sec/val"

data_transforms = {
    'train': transforms.Compose([
        transforms.Lambda(lambda x: x.unsqueeze(0)),
        transforms.Lambda(lambda x: x.repeat(3, 1, 1)),
        transforms.Normalize([0.485, 0.456, 0.406], [0.229, 0.224, 0.225])
    ]),
    'val': transforms.Compose([
        transforms.Lambda(lambda x: x.unsqueeze(0)),
        transforms.Lambda(lambda x: x.repeat(3, 1, 1)),
        transforms.Normalize([0.485, 0.456, 0.406], [0.229, 0.224, 0.225])
    ]),
}
train_dataset = torchvision.datasets.DatasetFolder(
    root=train_path,
    loader=npy_loader,
    extensions=('.npy',),
    transform=data_transforms['train']
)

val_dataset = torchvision.datasets.DatasetFolder(
    root=val_path,
    loader=npy_loader,
    extensions=('.npy',),
    transform=data_transforms['val']
)

train_dl = torch.utils.data.DataLoader(train_dataset, batch_size=batch_size, shuffle=True)
val_dl = torch.utils.data.DataLoader(val_dataset, batch_size=batch_size, shuffle=True)

criterion = nn.CrossEntropyLoss()

optimizer = torch.optim.SGD(model_conv.fc.parameters(), lr=LEARNING_RATE, momentum=0.9)

scheduler = torch.optim.lr_scheduler.StepLR(optimizer, step_size=7, gamma=0.1)

# Track metrics in these arrays
epoch_nums = []
training_loss = []
validation_loss = []

print('Training on', device)

dataloaders = {'train': train_dataset, 'val': val_dataset}
dataset_sizes = {'train': len(train_dataset), 'val': len(val_dataset)}

train_model(model_conv, criterion, optimizer, scheduler, num_epochs=num_epochs)
writer.flush()