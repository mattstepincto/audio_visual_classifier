import os
import numpy as np
import librosa
from tqdm import tqdm
import argparse

# Take lab file:
# Read folder with lab files, from each lab file use the same load_single_xy and save them as NUMPY into
# singing / not_singing folder based on the y value.


def get_npy_intervals(npy_folder, lab_files_folder, out_int_mel, sr, hl, f_size, f_overlap):
    output_directory = out_int_mel

    num_positive_intervals = 0
    num_negative_intervals = 0

    if not os.path.exists(output_directory):
        os.mkdir(output_directory)

    for data_npy_file in tqdm(os.listdir(npy_folder)):
        file_ = os.path.join(npy_folder, data_npy_file)
        load_single_xy(file_, lab_files_folder, num_positive_intervals, num_negative_intervals, f_size, f_overlap, sr, hl)


def load_label(audio_spec, audio_label_file, sr, hl):
    ''' Process and load label for the given audio
    Args:
        audio_spec : melgram of the audio. Shape=(n_bins, total_frames) ex.(80,14911)
        audio_label_file : path to the label file ex. './jamendo/jamendo_lab/02 - The Louis...lab'
    Return :
        lab : list of ground truth annotations per frame. Shape=(total_frames, )
    '''
    with open(audio_label_file, 'r') as f:
        total_frames = audio_spec.shape[1]
        label = np.zeros((total_frames,))

        for line in f:
            l = line.strip('\n').split(' ')
            start = librosa.time_to_frames(float(l[0]), sr=sr, hop_length=hl)
            end = librosa.time_to_frames(float(l[1]), sr=sr, hop_length=hl)

            is_vocal = 1 if l[2] == 'talking' or l[2] == '1' else 0

            label[start:end] = int(is_vocal)

    return label


def load_single_xy(audio_file, label_dir, num_positive_intervals, num_negative_intervals, f_size, f_overlap, sr, hl):
    ''' Create (melgram, label) data pair for the given audio

    Args:
        audio_file : name and path of the melgram npy file
        label_dir : path to the lab folder

    Return :
        audio_mel_feature : melgram of the audio. Shape=(80, total_frames)
        label : annotation for the audio. Shape=(total_frames,)

    :param label_dir:
    :param audio_file:
    :param f_overlap:
    :param f_size:
    :param num_negative_intervals:
    :param num_positve_intervals:
    '''

    # load audio
    audio_mel_feature = np.load(audio_file)  # (80, frame_len)
    label_filename = audio_file.split('/')[-1].replace('.npy', '.lab')
    label_filepath = os.path.join(label_dir, label_filename)
    all_labels_of_each_sample = load_label(audio_mel_feature, label_filepath, sr, hl)

    # count = 0
    for i in range(0, audio_mel_feature.shape[1] - f_size, f_overlap):
        x_segment = audio_mel_feature[:, i: i + f_size]
        # pick the center frame label
        y_label = all_labels_of_each_sample[i + int(f_size // 2) + 1]

        save_folder = ""
        if y_label == 1:
            save_folder = "talking"
            num_positive_intervals += 1
        else:
            save_folder = "not_talking"
            num_negative_intervals += 1


        # output_npy_dir = os.path.join(output_directory, save_folder)
        # if not os.path.exists(output_npy_dir):
        #     os.mkdir(output_npy_dir)
        #
        # output_filename = audio_file.split('/')[-1].split('.npy')[0] + "_intervalNum_" + str(i) + ".npy"
        # # if count == 2:
        # #     exit(0)
        # # count += 1
        # output_filename = os.path.join(output_npy_dir, output_filename)
        # np.save(output_filename, x_segment)


# Load the npy files from the singing /not_singing folders
# Train the model

def main():

    lab_files_folder = "/home/ubuntu/AM_Talking_v1/data/all_lab_files"  # contains all LAB files
    npy_folder = "/home/ubuntu/AM_Talking_v1/data/mel_files/val"
    output_folder = "/home/ubuntu/AM_Talking_v1/data/mel_intervals_25sec/val"

    HOP_LENGTH = 315
    SR = 22050

    CNN_INPUT_SIZE = 38
    CNN_OVERLAP = 47  # Hopsize of 5 for training, 1 for inference


    parser = argparse.ArgumentParser()
    parser.add_argument('-npy', dest='npy_folder', help='npy_folder absolute path')
    parser.add_argument('-lab',dest='lab_files_folder' ,help='lab folder absolute path')
    parser.add_argument('-out_int_mel', dest='out_int_mel', help='output directory of the mel intervals')
    parser.add_argument('-sr', dest='out_int_mels', help='sample rate')
    parser.add_argument('-hl', dest='out_int_mels', help='hop length')
    parser.add_argument('-f_size', dest='out_int_mels', help='frame size ')
    parser.add_argument('-f_overlap', dest='out_int_mels', help='overlapping size')

    args = parser.parse_args()


    get_npy_intervals(args.npy_folder, args.lab_files_folder, args.out_int_mel, args.sr, args.hl, args.f_size, args.f_overlap)



if __name__ == '__main__':
    main()