import json
import os
import argparse
from tqdm import tqdm
import boto3
import subprocess
from tinytag import TinyTag
import math

s3 = boto3.client('s3')


def get_audio_from_mp4(audio_file_path, video_path, txt_not_download_absolute_path, retrieved_audio):
    command = f"ffmpeg -loglevel error -i '{video_path}' '{audio_file_path}'"

    try:
        subprocess.check_output(command, shell = True, stderr=subprocess.STDOUT)
        retrieved_audio += 1

    except subprocess.CalledProcessError as e:
        f = open(txt_not_download_absolute_path, 'a')
        f.write(f"{video_path} \n")
        print(f"-- Error, unexpected character --")
    # subprocess.call(command, shell=True)
    return retrieved_audio


def download_videos_from_jsonl_and_get_audio(jsonl_file_absolute_path, dir_audio_output, txt_not_download_absolute_path, possible_videos_to_retrieve, retrieved_audio):

    with open(jsonl_file_absolute_path) as f:
        data = json.loads("[" + f.read().replace("}\n{", "},\n{") + "]")

    for row in tqdm(data):
        if row['answer'] != 'accept':
            continue

        if 'audio_spans' not in row.keys():
            continue

        if len(row['audio_spans']) <= 0:
            continue

        possible_videos_to_retrieve += 1

        video_absolute_path = (row['meta']['path']+'.mp4').replace('/home/ubuntu/HDD_data/','/home/ubuntu/HDD_data/scraper/')
        output_dir_video = '/'.join(video_absolute_path.split('/')[:-1])

        if not os.path.exists(output_dir_video):
            os.makedirs(output_dir_video, exist_ok=True)

        if not os.path.exists(video_absolute_path):

            file_to_download = '/'.join(video_absolute_path.split('/')[3:])
            s3.download_file('client-livelivelive-bigroom',
                             file_to_download,
                             video_absolute_path)

        audio_filename = (row['meta']['path']+'.wav').split('/')[-1]
        audio_absolute_path = os.path.join(dir_audio_output, audio_filename)

        if not os.path.exists(audio_absolute_path):
            try:
                retrieved_audio = get_audio_from_mp4(audio_absolute_path, video_absolute_path, txt_not_download_absolute_path, retrieved_audio)
            except ValueError:
                print("-------- ERROR: Cannot get wav file, skip --------")
                return False
    return possible_videos_to_retrieve, retrieved_audio


def get_audio_len(output_txt, dir_audio_output):

    """
    Function that creates a text file with the duration of the audio files from the jsonl file.
    The duration is retrieved from the tags of the mp3, the process is quite fast because
    no audio reading is performed.

    Inputs:
    data_dir -> txt file created in the data_dir path
    src_dir ->  source directory with all the mp3 files

    Output:
    txt file created in the data_dir path
    """


    with open(output_txt, "w") as file:
        for audio_file in os.listdir(dir_audio_output):
            if audio_file.endswith('.wav'):
                tag = TinyTag.get(os.path.join(dir_audio_output, audio_file))
                file.write(str(os.path.join(dir_audio_output, audio_file)) + ' ' + str(tag.duration) + '\n')
    file.close()
    return output_txt


def recursive_merge(inter, start_index=0):

    for i in range(start_index, len(inter) - 1):
        if inter[i][1] > inter[i+1][0]:
            new_start = inter[i][0]
            new_end = inter[i+1][1]
            inter[i] = [new_start, new_end]
            del inter[i+1]
            return recursive_merge(inter.copy(), start_index=i)

    return inter


def get_end_start_audio_spans(len_audio_txt_absolute_path):

    # in order to get total duration and take into account start and end spans
    audio_files_names = []
    audio_files_len = []

    with open(len_audio_txt_absolute_path) as infile:
        for line in infile:
            audio_files_names.append(line.split(' ')[0])
            audio_files_len.append(line.split(' ')[1])

    print(f'{len_audio_txt_absolute_path} loaded!')
    return audio_files_names, audio_files_len


def compute_files(merged, audio_absolute_path, audio_filename, lab_output, audio_files_names, audio_files_len):

    """
    Fixing for first spans and last span

    This part of the script retrieve those audio spans that are at the end and at the beginning of the
    audio and are not annotated. For example:

    We retrieved from the jsonl file the following annotations

    from 1.00sec to 2.00sec is singing
    from 3.00sec to 4.00sec is singing

    We compute the no_singing annotations between singing annotations. Following the previous examples;
    we would retrieve the no_singing label between 2.00 and 3.00 seconds. However, no_singing labels
    between 0sec - 1sec and 4sec till the end, are not retrieved. That's why we need to retrieve the actual len of
    the audio files, in order to retrieve this missing parts.
    """

    final_merged = []
    tmp = None
    count = 0
    for iii in merged:
        if tmp is not None:
            if iii[0] - tmp:

                final_merged.append([[tmp, iii[0]], 'not_talking'])
        final_merged.append([iii, 'talking'])

        tmp = iii[1]
        count += 1

    if final_merged[0][0][0] != 0:
        final_merged = [[[0, final_merged[0][0][0]],'not_talking']] + final_merged

    # fix for last spans

    if audio_absolute_path in audio_files_names:
        len_of_filename = math.floor(float(audio_files_len[audio_files_names.index(audio_absolute_path)]))
        # append last span
        # add 1 as a th in order to retrieve relevant results and not eg. 0.1 seconds not sign spans at the end.

        if final_merged[-1][0][-1] + 1 < len_of_filename:
            final_merged.append([[final_merged[-1][0][-1], len_of_filename],'not_talking'])

    output_file = os.path.join(lab_output, audio_filename.replace('.wav','.lab'))


    with open(output_file, "w") as file:
        for times in final_merged:
            file.write(str('%.3f' % times[0][0]) + ' ' + str('%.3f' % times[0][1]) + ' ' + times[1] + '\n')
        file.close()


def get_lab_files(jsonl_file_absolute_path, dir_audio_output, len_audio_txt_absolute_path, lab_output):

    audio_files_names, audio_files_len = get_end_start_audio_spans(len_audio_txt_absolute_path)
    number_of_files_processed = 0

    with open(jsonl_file_absolute_path) as f:
        data = json.loads("[" + f.read().replace("}\n{", "},\n{") + "]")

    for row in tqdm(data):
        overall_time_spans = []

        if row['answer'] != 'accept':
            continue

        if 'audio_spans' not in row.keys():
            continue

        if len(row['audio_spans']) <= 0:
            continue

        for time in row['audio_spans']:

            start_time = time['start']
            end_time = time['end']

            if start_time < 0:
                start_time = 0

            spans = [start_time, end_time]
            overall_time_spans.append(spans)
            overall_time_spans = sorted(overall_time_spans)

        merged = recursive_merge(overall_time_spans)

        if time in row['audio_spans']:

            audio_filename = (row['meta']['path'] + '.wav').split('/')[-1]
            audio_absolute_path = os.path.join(dir_audio_output, audio_filename)

            compute_files(merged, audio_absolute_path, audio_filename, lab_output, audio_files_names, audio_files_len)
            number_of_files_processed += 1

        if number_of_files_processed % 50 == 0:
            print(f'number of files processed: {number_of_files_processed}')

    print("TOTAL files processed, ", number_of_files_processed)


def main():

    parser = argparse.ArgumentParser()
    parser.add_argument('-s', dest='jsonl_dir_absolute_path',
                        help='jsonl_dir_absolute_path.')
    parser.add_argument('-o_au', dest='dir_audio_output', help='directory that contains the audio files.')
    parser.add_argument('-txt', dest='output_txt', help='output_txt')
    parser.add_argument('-o_lab', dest='lab_output', help='lab_output')

    args = parser.parse_args()

    txt_not_download_absolute_path = os.path.join(args.output_txt, "output_not_download.txt")
    f = open(txt_not_download_absolute_path, 'w')

    txt_len_audio_absolute_path = os.path.join(args.output_txt, "output_len_files.txt")
    len_audio_txt_absolute_path = get_audio_len(txt_len_audio_absolute_path, args.dir_audio_output)

    test_videos = []  #
    possible_videos_to_retrieve = 0
    retrieved_audio = 0

    # for jsonl_file in os.listdir(args.jsonl_dir_absolute_path):
    #     print(f'----------- Downloading videos and audio from {jsonl_file} -----------')
    #     jsonl_file_absolute_path = os.path.join(args.jsonl_dir_absolute_path, jsonl_file)
    #     print(download_videos_from_jsonl_and_get_audio(jsonl_file_absolute_path, args.dir_audio_output, txt_not_download_absolute_path, possible_videos_to_retrieve, retrieved_audio))
    #

    for jsonl_file in os.listdir(args.jsonl_dir_absolute_path):
        print(f'----------- Retrieve lab files from: {jsonl_file} -----------')
        jsonl_file_absolute_path = os.path.join(args.jsonl_dir_absolute_path, jsonl_file)
        get_lab_files(jsonl_file_absolute_path, args.dir_audio_output, len_audio_txt_absolute_path, args.lab_output)
        #get_audios


if __name__ == '__main__':
    main()