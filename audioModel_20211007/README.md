# Audio model (A) Singing - Talking classification models

This folder contains the files and documentation related to the singing and talking classification. The training phase of the models is computed by using Python, specifically, we are using PyTorch in order to compute the feature extraction and model it self.

The audio model is based on *Resnet18* and *Mel-spectrograms* have been used as the feature.

## 1. Pre-processing of audio data and retrieving of mel spectrogram data

The python script `A_retrieve_data__preprocess_melspectrograms.py` contains the process in order to retrieve and preprocess the audio files and get the melspectrograms. It takes the audio files from the `audio_dir` directory and retrieve the spectrograms and save them into the `mel_dir` directory.

```
 arguments:
  -h, --help            show this help message and exit
  -mel_dir MEL_DIR      output mel directory - absolute path
  -audio_dir AUDIO_DIR  input audio directory - absolute path
  -sr SR                downsample - sample rate eg 22050
  -hl HL                hoplength mel spectrograms - eg 315
  -n_fft N_FFT          fft sample len eg 1024
  -n_mels N_MELS        number of melspectrograms bins - eg 80
  -f_max F_MAX          f_max max audio freq for melspectrograms computation
                        eg 8000
  -f_min F_MIN          f_min min audio freq for melspectrograms 27.5

``` 
     
Example: 

``` 
python3 A_retrieve_data__preprocess_melspectrograms.py  -mel_dir [absolute_path_mel_dir] -audio_dir [absolute_path_audio_dir] -sr 22050 -hl 315 -n_fft 1024 -n_mels 80 -f_max 8000 -f_min 27 
```

## 2. Retrieve annotated data and merge with the spectrograms

The python script `retrieve_talking_annotations.py` implements the retrieving of the annotated data from the jsonl files from the prodigy annotation tool. The script takes as main input the directory that contains the jsonl files and the related audio files. For each annotated file it outputs a `.lab` file containing the annotations per time span eg: 0 10 not_talking.

Steps: 

- Retrieve and save the duration of the audio files, output file: `output_len_files.txt`.
- For each annotated file found in the jsonl file we save its annotations. The annotations are save in the `o_lab` directory `.lab` extension files. 

During this last step we use the duration of the audio files (saved previously) to compute the last annotation of each file. Ie, the annotations found in the jsonl files contain the **talking** time spans of an event, it does not contain the **not_talking** ones, therefore these events are computed by retrieving the gap between two talking events. If the last event is not_talking we need the duration of the audio in order to retrieve this last part.

```
 arguments:
  -h, --help            show this help message and exit
  -s JSONL_DIR_ABSOLUTE_PATH
                        jsonl_dir_absolute_path.
  -o_au DIR_AUDIO_OUTPUT
                        directory that contains the audio files.
  -txt OUTPUT_TXT       output_txt
  -o_lab LAB_OUTPUT     lab_output

```

Example:

``` 
python3 retrieve_talking_annotations.py -s [absolute_path_of_the_directory_that_contains_jsonl_files] -o_au []  -txt [] -o_lab [] 
```

**Important**: this script also contains a function that downloads those audio files found in the annotations jsonl file. Function name: `download_videos_from_jsonl_and_get_audio`

Example: 

```
download_videos_from_jsonl_and_get_audio(jsonl_file_absolute_path, args.dir_audio_output, txt_not_download_absolute_path, possible_videos_to_retrieve, retrieved_audio)
```

The python script `get_npy_interval_files.py` merges both information retrieved: the lab files and the melspectrograms. It splits each numpy file (melspectrogram) into equal duration intervals and save them into talking and not_talking folders, depending on the corresponding label found.  

```
arguments:
  -h, --help            show this help message and exit
  -npy NPY_FOLDER       npy_folder absolute path
  -lab LAB_FILES_FOLDER
                        lab folder absolute path
  -out_int_mel OUT_INT_MEL
                        output directory of the mel intervals
  -sr OUT_INT_MELS      sample rate
  -hl OUT_INT_MELS      hop length
  -f_size OUT_INT_MELS  frame size
  -f_overlap OUT_INT_MELS
                        overlapping size

```

Finally we end up with two folders; Talking / Not_talking. Each folder will contain npy files with the same length, most likely the number of examples is unbalanced, so we can apply over/under sampling to balance it out. 

## 4. Training

**THE PATHS IN THIS SCRIPT ARE HARDCODED**

The script `A_v3.0.0_train.py` is used to train the model by using the data retrieved in the previous steps. 

This script takes the train and val directories that contain the Talking and Not_talking folders with the npy examples. The examples are loaded by using a dataloader. We also apply some transformations in order to adjust the data to the Resnet18 pretrained model. 

We then add a linear output at the end of the model and earlystopping methods are also implemented. Each checkpoint is automatically saved at the end of an epoch as well as the accuracies and losses.

 