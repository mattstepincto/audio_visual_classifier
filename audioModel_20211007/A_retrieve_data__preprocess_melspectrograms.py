import os
import torchaudio
import torch
from tqdm import tqdm
import numpy as np
import argparse


def resample_audio(signal, sr, SAMPLE_RATE, device):
    if sr != SAMPLE_RATE:
        resampler = torchaudio.transforms.Resample(sr, SAMPLE_RATE).to(device)
        signal = resampler(signal)
    return signal


def mix_down(signal):
    if signal.shape[0] > 1:
        signal = torch.mean(signal, dim=0, keepdim=True)
    return signal


def retrieve_melspectrograms_from_audio_directory(mel_dir, audio_dir, SAMPLE_RATE, hl, n_fft, n_mels, f_max, f_min):

    if torch.cuda.is_available():
        device = "cuda"
    else:
        device = "cpu"
    print(f"Using device {device}")

    mel_spectrogram = torchaudio.transforms.MelSpectrogram(
        sample_rate=int(SAMPLE_RATE),
        n_fft=int(n_fft),
        hop_length=int(hl),
        n_mels=int(n_mels),
        f_max=int(f_max),
        f_min=int(f_min)
    ).to(device)

    for audio_file in tqdm(os.listdir(audio_dir)):

        if os.path.exists(os.path.join(mel_dir, audio_file.replace('.wav', '.npy'))):
            continue

        signal, sr = torchaudio.load(os.path.join(audio_dir, audio_file))
        signal = signal.to(device)
        signal = resample_audio(signal, sr, int(SAMPLE_RATE), device)
        signal = mix_down(signal)
        audio_mel_feature = mel_spectrogram(signal)

        audio_mel_feature = audio_mel_feature.detach().cpu().numpy()
        audio_mel_feature = np.ndarray.squeeze(audio_mel_feature, axis=0)
        np.save(os.path.join(mel_dir, audio_file.replace('.wav', '.npy')), audio_mel_feature)


def main():

    parser = argparse.ArgumentParser()
    parser.add_argument('-mel_dir', dest='mel_dir', help='output mel directory - absolute path')
    parser.add_argument('-audio_dir', dest='audio_dir', help='input audio directory - absolute path')

    parser.add_argument('-sr', dest='sr', help='downsample - sample rate eg 22050')
    parser.add_argument('-hl', dest='hl', help='hoplength mel spectrograms - eg 315')
    parser.add_argument('-n_fft', dest='n_fft', help='fft sample len eg 1024')
    parser.add_argument('-n_mels', dest='n_mels', help='number of melspectrograms bins - eg 80')
    parser.add_argument('-f_max', dest='f_max', help='f_max max audio freq for melspectrograms computation eg 8000')
    parser.add_argument('-f_min', dest='f_min', help='f_min min audio freq for melspectrograms 27')

    args = parser.parse_args()

    print('Main running...')
    retrieve_melspectrograms_from_audio_directory(args.mel_dir, args.audio_dir, args.sr, args.hl, args.n_fft, args.n_mels, args.f_max, args.f_min)

if __name__ == '__main__':
    main()