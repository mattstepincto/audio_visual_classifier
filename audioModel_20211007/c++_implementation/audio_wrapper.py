#!/usr/bin/env python3
"""
Create a data preprocess pipeline that can be run with libtorchaudio
"""
import os
import argparse

import numpy as np

import torch
import torchaudio
import torchvision

torch.set_printoptions(precision=10)


class Pipeline(torch.nn.Module):
    """Example audio process pipeline.
    This example load waveform from a file then apply effects and save it to a file.
    """
    def __init__(self, rir_path: str):
        super().__init__()

        self.mel_spectrogram = torchaudio.transforms.MelSpectrogram(
            sample_rate=22050,
            n_fft=1024,
            hop_length=315,
            n_mels=80,
            f_max=8000.0,
            f_min=27.0
        )

        self.norm = torchvision.transforms.Normalize([0.485, 0.456, 0.406], [0.229, 0.224, 0.225])

    def forward(self, input_buffer_data):
        input_buffer_data = input_buffer_data.float()
        mel_spec = self.mel_spectrogram(input_buffer_data)

        x_segment = mel_spec.unsqueeze(0)
        x_segment = x_segment.repeat(3, 1, 1)
        x_segment = x_segment.unsqueeze(0)

        norm_ = self.norm(x_segment)
        return norm_

def _create_jit_pipeline(rir_path, output_path):
    module = torch.jit.script(Pipeline(rir_path))
    print("*" * 40)
    print("* Pipeline code")
    print("*" * 40)
    print()
    print(module.code)
    print("*" * 40)
    module.save(output_path)


def _get_path(*paths):
    return os.path.join(os.path.dirname(__file__), *paths)


def _parse_args():
    parser = argparse.ArgumentParser(description=__doc__)
    parser.add_argument(
        "--rir-path",
        default=_get_path("..", "data", "rir.wav"),
        help="Audio dara for room impulse response."
    )
    parser.add_argument(
        "--output-path",
        default=_get_path("view_size.zip"),
        help="Output JIT file."
    )
    return parser.parse_args()


def _main():
    args = _parse_args()
    _create_jit_pipeline(args.rir_path, args.output_path)


if __name__ == '__main__':
    _main()