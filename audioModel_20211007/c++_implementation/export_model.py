import torch

# An instance of your model.
audio_model_path = "C:/Users/Ignas/Desktop/bigroom-models/audioModel_20211007/c++_implementation/e18_i1758.pth"
model = torch.load(audio_model_path, map_location=torch.device('cpu'))
script_torch = torch.jit.script(model)
example = torch.rand(1, 3, 80, 38)

model.eval()

script_torch.save("e18_i1758.pt")
