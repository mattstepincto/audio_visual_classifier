# Audio model (A) - Talking - C++ implementation

The following code is based on the libtorchaudio repo from here: https://github.com/pytorch/audio/tree/main/examples/libtorchaudio.

After building the libtorchaudio examples in the EC2 instance the CmakeLists.txt has been placed in audio/examples/libtorchaudio/augmentation/ . You can then run and build the repo by following the instructions that are in github. Take into account that if you want to use Xcode instead of -GNinja, you should use -G Xcode.

- audio_wrapper.py -> Creates a pipeline that can be run in libtorchaudio
- export_model.py -> Takes the model and export it into a pt format in order to be readable with libtorch.
- e6_i393.pth -> the model itself
- circular_buffer.cpp -> circular buffer implementation
- AudioFIle.h -> library used to load the audio wav file
- main.cpp -> The main cpp file in order to run and load the wrapped files. It takes 2 arguments: a zip file (audio_wrapper output) and the pt model (the export_model output).

This implementation produces the time span ( according to the audio file) and its prediction. It takes a wav audio file, once the buffer is full of audio data, calculates the related mel-spectrogram using the wrapped python script. It then returns the mel-spectrogram to C++. It then loads the mel-spectrograms into the wrapped libtorch model and prints the predictions. 