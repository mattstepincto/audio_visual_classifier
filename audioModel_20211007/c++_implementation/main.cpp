#include <torch/script.h>
#include <torch/torch.h>
#include "circular_buffer.cpp"
//#include "WaveFile.hpp"
#include "AudioFile.h"
//#include "/Users/ignasinou/Desktop/FIVE_VECTORS/season_2/C++_libtorch_wrapper/audio/examples/libtorchaudio/AudioFile.h"


#define NUM_SAMPLES_BETWEEN_SPECTS 14805

int main(int argc, char* argv[]) {
    
    std::cout << argc << std::endl;
    if (argc !=3) {
    std::cerr << "Usage: " << argv[0] << "<JIT_OBJECT>" << std::endl;

    return -1;
    }
    torch::jit::script::Module module;
    torch::jit::script::Module module2;
    module2 = torch::jit::load(argv[2]);

    std::cout << "Loading module from: " << argv[1] << std::endl;
    try {
    module = torch::jit::load(argv[1]);

    } catch (const c10::Error &error) {
    std::cerr << "Failed to load the module:" << error.what() << std::endl;
    return -1;
    }

    int samples_buffer = 11969; //38 frames in the spectrogram. 38 * 315 (hopsize) = 11970.
    circular_buffer<float*> circle(samples_buffer);
    
    AudioFile<float> audioFile;
    
    std::string audio_path = "/home/ubuntu/c++_test/evaluation_scripts/audio_test/BillieEilish_talking_model_trim_resample.wav";
//    std::string audio_path = "/Users/ignasinou/Desktop/FIVE_VECTORS/season_2/scripts/season_2_scripts/AM_Talking_v1/retrieve_data/audio_test/BillieEilish_talking_model_trim_resample.wav";
    
    audioFile.load(audio_path);
    
    auto SR = audioFile.getSampleRate();
    
    int countbuff = 0;
    for (int ii = 0; ii < audioFile.getNumSamplesPerChannel(); ++ii){
//        std::cout << audioFile.samples[0][ii] << std::endl;

        circle.put(&audioFile.samples[0][ii]);
        
        
        if (circle.full()){
            if ((countbuff % NUM_SAMPLES_BETWEEN_SPECTS) == 0){ // spectrograms are separated by 3000 samples.
//                std::cout << circle.get() << std::endl;
                auto input_tensors = torch::from_blob(circle.get(), {samples_buffer}).clone();
                
//                auto input_tensors = torch::tensor(*circle.get());
                
                auto output_spect = module.forward({input_tensors});
                module2.eval();
                at::Tensor output = module2.forward({output_spect}).toTensor();
                at::Tensor output_sm = torch::softmax(output, 1);
                std::tuple<at::Tensor, at::Tensor> result = torch::max(output_sm, 1);
                
                    
                if (std::get<1>(result).item<bool>() == 0) {
                    std::cout << (float)countbuff/(float)SR << " " << (float)(samples_buffer+countbuff)/(float)SR << " Not talking" <<  "\n";
                } else {
                    std::cout << (float)countbuff/(float)SR << " " << (float)(samples_buffer+countbuff)/(float)SR << " Talking" << "\n";
                }
            }
            countbuff++;
        }
    }

}

