
#define OPENCV 1
#include <stdlib.h>
#include <fstream>
#include <iostream>
#include "include/AudioFile.h"
#include "circularBuffer/circular_buffer.cpp"
#include "VideoSide.h"
#include <torch/torch.h>
#include <torch/script.h>
#include <QCoreApplication>


// Debug mode saved the predicted frames and videos to disk
#define DEBUG_MODE
#define SAMPLERATE 22050
#define FRAMES_BETWEEN_SPECTS 10
#define NUM_SAMPLES_BETWEEN_SPECTS 14805

float * load_audiofile(AudioFile < float > & sound, std::string file_path);
QByteArray convert_array_to_qbytearray(float * pcm_array, int n_samples);
float * convert_qbytearray_to_array(QByteArray qbyte_array, int n_samples);
void compare_arrays(float * old_pcm_array, float * new_pcm_array, int n_samples);

bool perform_infernce(QByteArray qbyte_array, int numSamplesRetrieve, torch::jit::script::Module module, torch::jit::script::Module module2) {

    // convert the qbyte array into a double array - for client to use
    float * new_pcm_array = convert_qbytearray_to_array(qbyte_array, numSamplesRetrieve);

    // compare_arrays(short_pcm_array, new_pcm_array, numSamplesRetrieve);
    auto input_tensors = torch::from_blob(&new_pcm_array[0], { numSamplesRetrieve }).clone();

    //auto input_tensors = torch::from_blob(circle.get(), { numSamplesRetrieve }).clone();
    auto output_spect = module.forward({ input_tensors });
    module2.eval();
    at::Tensor output = module2.forward({ output_spect }).toTensor();
    at::Tensor output_sm = torch::softmax(output, 1);
    std::tuple<at::Tensor, at::Tensor> result = torch::max(output_sm, 1);

    bool result_bool = std::get<1>(result).item<bool>();

    return result_bool;
}

int main(int argc, char* argv[]) {
    std::cout << "Running inference..." << std::endl;

    std::string video_file = argv[1];
    std::string audio_path = argv[2];
    std::string out_file = argv[3];

    std::string wts_file = argv[4]; 
    std::string engine_file = "yolov5-large-livelivelive-stage-3cls-20210730_best.engine"; // engine filaname
    std::string python_module = argv[5]; // Python module spectrograms
    std::string audio_model_pt = argv[6]; // python audio model
    
    // ------------------------ LOAD LIBTORCH MODULES ---------------------

    torch::jit::script::Module module;
    std::cout << "Loading module from: " << python_module << std::endl;
    try {
        module = torch::jit::load(python_module);
    }
    catch (const c10::Error &error) {
        std::cerr << "Failed to load the module:" << error.what() << std::endl;
        return -1;
    }

    torch::jit::script::Module module2;
    module2 = torch::jit::load(audio_model_pt);

    // ------------------------ LOAD LIBTORCH MODULES ---------------------

    bool createEngine;

    std::ifstream fin(engine_file);

    if (fin.is_open()) {
        std::cout << "Engine Found " << engine_file << std::endl;
        createEngine = false;
    }
    else {
        std::cout << "Engine Not Found " << engine_file << std::endl;
        createEngine = true;
    }
    // ---------------------------- VIDEO ------------------------------- //

    TrtLoader trtLoader = LoadEngineVideo(engine_file, wts_file, createEngine);

    cv::VideoCapture cap = cv::VideoCapture();
    int retval = cap.open(video_file);
    if (!cap.isOpened()) {
        std::cout << "ERROR! Unable to open video\n";
        return -1;
    }

    cv::Mat image;
    int FPS = 15; // Hardcoded FPS, used to sync audio and video
    cv::VideoWriter outputVideo;
    int fourcc = outputVideo.fourcc('M', '2', '6', '4');
    int ex = static_cast<int>(cap.get(cv::CAP_PROP_FOURCC));
    cv::Size S = cv::Size((int)cap.get(cv::CAP_PROP_FRAME_WIDTH), (int)cap.get(cv::CAP_PROP_FRAME_HEIGHT));
    outputVideo.open(out_file, fourcc, cap.get(cv::CAP_PROP_FPS), S, true);
    if (!outputVideo.isOpened())
    {
        std::cout << "Could not open the output video for write: " << std::endl;
        return -1;
    }

    // ---------------------------- VIDEO ------------------------------- //

    // ---------------------------- LOAD AUDIO ------------------------------- //

    // Setup AudioFile object
    AudioFile < float > sound;

    // load the Audio into a double array
    float *pcm_array = load_audiofile(sound, audio_path);

    std::vector<float> time_dur;
    int fcount = 0;
    int sampleRate = sound.getSampleRate();
    int numSamplesRetrieve = 11969;
    int countbuff = 0;

    // initialise short pcm array (will be used for inference)
    float *short_pcm_array = new float[numSamplesRetrieve]();

    // Here we are assigning the block of memory in short_qbyte_array to point to short_pcm_array.
    // The client will use a short_qbyte_array as input to the function below
    // QByteArray short_qbyte_array = convert_array_to_qbytearray(short_pcm_array, numSamplesRetrieve);

    float currentFrameInSamples = 0;
    int start_excerpt = 0;
    int resAudio = 0;
    int output_label;
    int mm = 0;
    int nn = 0;
    bool result = false;


    //Start reading frames.

    while (cap.read(image)) {
        fcount++;

        Prediction out_pred = predict_frame(trtLoader, image);

        // Take the related float audio array
        currentFrameInSamples = ((float)fcount / (float)FPS) * sampleRate;
        start_excerpt = currentFrameInSamples - numSamplesRetrieve;


        //if (circle.full()) {
        if (currentFrameInSamples > numSamplesRetrieve) {

            mm = 0;

            for (nn = start_excerpt; nn < currentFrameInSamples; nn++) {
                short_pcm_array[mm] = pcm_array[nn];
                mm = mm + 1;
            }

            QByteArray qbyte_array = convert_array_to_qbytearray(short_pcm_array, numSamplesRetrieve);

            result = perform_infernce(qbyte_array, numSamplesRetrieve, module, module2);

            if (result == 0) {
                std::cout << ((float)currentFrameInSamples - (float)numSamplesRetrieve) / (float)sampleRate << " " << (float)currentFrameInSamples / (float)sampleRate << " Not singing" << "\n";
                output_label = 1;
            }
            else {
                std::cout << ((float)currentFrameInSamples - (float)numSamplesRetrieve) / (float)sampleRate << " " << (float)currentFrameInSamples / (float)sampleRate << " Singing" << "\n";
                output_label = 2;
            }
        }

        auto boxes = (*out_pred.boxes);
        auto labels = (*out_pred.labels);
        auto labels_text = (*out_pred.s_ns_class);
        auto det_scores = (*out_pred.det_scores);

        // /using namespace cv;
        cv::Size size = image.size();
        int height = size.height;
        int width = size.width;

        for (int i = 0; i < boxes.size(); i++) {
            auto box = boxes[i];
            std::string label_box_output;
            cv::Rect r = get_rect(image, box);
            cv::rectangle(image, r, cv::Scalar(0x27, 0xC1, 0x36), 2);

            // Add audio correction:
            // if resAudio not singing but label is singing -> label => not_singing
            cv::Scalar color_output;
            if (std::to_string((int)labels[i]) == "0") {
                label_box_output = "person";
                color_output = cv::Scalar(255, 255, 255);
            }
            else if (std::to_string((int)labels[i]) == "1") {
                label_box_output = "face_singing";
                color_output = cv::Scalar(0, 255, 100);
            }
            else if (std::to_string((int)labels[i]) == "2") {
                label_box_output = "face_not_singing";
                color_output = cv::Scalar(255, 0, 0);
            }

            if (output_label == 1 && (int)labels[i] == 1) {
                cv::putText(image, "face_not_singing", cv::Point(r.x, r.y - 1), cv::FONT_HERSHEY_PLAIN, 1.2, cv::Scalar(255, 0, 0), 2);
            }
            else {
                cv::putText(image, label_box_output, cv::Point(r.x, r.y - 1), cv::FONT_HERSHEY_PLAIN, 1.2, color_output, 2);
            }
        }

        outputVideo.write(image);
        time_dur.push_back((*out_pred.det_times)[0]);

        //auto time_dur = (*out_pred.det_times);
        std::cout << "\nOverall: Time for (detector) first image (milliseconds): " << time_dur[0] << std::endl;
        float total_time = 0.0;
        for (int j = 1; j < time_dur.size(); j++) {
            total_time += time_dur[j];
        }
        std::cout << "Overall: Average time per image (for detector) after first image (milliseconds): " << total_time / (time_dur.size() - 1) << std::endl;
        std::cout << "Overall: FPS: " << (1000) / (total_time / (time_dur.size() - 1)) << std::endl;
    }
}

float * load_audiofile(AudioFile < float > & sound, std::string file_path) {

    sound.load(file_path);
    int n_samples = sound.getNumSamplesPerChannel();
    int n_channels = sound.getNumChannels();

    // Convert to 2D vector to 1D QByteArray by averaging channels
    float * pcm_array = new float[n_samples]();

    // Average channels
    for (int sample = 0; sample < n_samples; sample++) {
        for (int channel = 0; channel < n_channels; channel++) {
            pcm_array[sample] += sound.samples[channel][sample];
        }
        pcm_array[sample] /= n_channels;
    }
    return pcm_array;
}
QByteArray convert_array_to_qbytearray(float * pcm_array, int n_samples) {

    printf("converting float array into qbyte_array..\n");

    // Convert from 1D float array into QByteArray
    QByteArray qbyte_array = QByteArray::fromRawData(reinterpret_cast <
        const char *> (pcm_array), n_samples * sizeof(float));

    return qbyte_array;
}

float * convert_qbytearray_to_array(QByteArray qbyte_array, int n_samples) {

    printf("converting qbyte_array into double array..\n");
    // Convert from 1D QByteArray back to float array
    float * new_pcm_array = new float[n_samples]();
    new_pcm_array = reinterpret_cast <float *> (qbyte_array.data());
    return new_pcm_array;
}

void compare_arrays(float * old_pcm_array, float * new_pcm_array, int n_samples) {
    printf("testing the arrays before and after qbyte_array conversion..\n");
    // Calculate the difference between input float array and output float array
    double diff = 0;
    for (int sample = 0; sample < n_samples; sample++) {
        diff += old_pcm_array[sample] - new_pcm_array[sample];
        printf("old_pcm_array[sample] = %f ", old_pcm_array[sample]);
        printf("new_pcm_array[sample] = %f\n", new_pcm_array[sample]);
        assert(diff == 0);
    }
    printf("total diff = %f\n", diff);

    if (diff == 0)
        printf("Both conversions successful!\n");
}
