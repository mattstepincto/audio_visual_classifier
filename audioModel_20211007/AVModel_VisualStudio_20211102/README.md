# AVModel C++ implementation - Visual Studio 2017 15

Please refer the original [tennsorrtx](https://github.com/wang-xinyu/tensorrtx/tree/master/yolov5) for the reference implementation

The demo does the following:

1. Builds a TensorRT engine and saves it to the provided path.
2. Loads the engine build in Step1.
3. Runs the inference - detector runs on the de-serealized TensorRT engine: Yolov5 + singing classifer.
4. Saves the test output video with detection to the provided filename.

## Model - Inputs/Details

- Input audio sample rate should be 22050 Hz.
- The perform_infernce function in AV_model_202111.cpp takes a Qbyte array 11969 samples long, and performs and outputs the result of the infernce.
- Yolo5 results and audio results are then combined in order to reduce the false positeve rate. 

### Part 1 - install libraries

- Install CMAKE 3.16.4 https://github.com/Kitware/CMake/releases/download/v3.16.4/cmake-3.16.4-win64-x64.msi
- Install Microsoft Visual Studio 2019 v16 https://docs.microsoft.com/en-us/visualstudio/releases/2019/release-notes
When installing, select the package: "Desktop dev with c++"

- Install CUDA v11.1: https://docs.nvidia.com/cuda/pdf/CUDA_Installation_Guide_Windows.pdf
https://developer.nvidia.com/cuda-11.1.1-download-archive

- Download CUDA Toolkit 11.1:
https://developer.nvidia.com/cuda-11.1.0-download-archive?target_os=Windows&target_arch=x86_64&target_version=10&target_type=exenetwork

Install cuDNN
Download the cuDNN Libraries: from here: https://developer.nvidia.com/compute/machine-learning/cudnn/secure/8.0.5/11.1_20201106/cudnn-11.1-windows-x64-v8.0.5.39.zip
Visit the official website: https://developer.nvidia.com/rdp/cudnn-download
Click “I Agree To the Terms of the cuDNN Software License Agreement”
Click “Download cuDNN v8.0.5 (November 9th, 2020), for CUDA 11.1” 
Click “cuDNN Library for Windows (x86)”

- Download TensorRT 7.2.1.6:https://developer.nvidia.com/compute/machine-learning/tensorrt/secure/7.2.1/zip/TensorRT-7.2.1.6.Windows10.x86_64.cuda-11.1.cudnn8.0.zip

- Install TensorRT 7.2.1.6: https://docs.nvidia.com/deeplearning/tensorrt/install-guide/index.html#installing-zip
- Install OpenCV 4.3.0: https://kumisystems.dl.sourceforge.net/project/opencvlibrary/4.3.0/opencv-4.3.0-vc14_vc15.exe
- Install Torch 1.10+cu11.1 : https://download.pytorch.org/libtorch/cu111/libtorch-win-shared-with-deps-1.10.0%2Bcu111.zip
- Install Qt 5.15

**NOTE**: Ensure all above libraries are added to Path

### Part 2 - configure and build yolov5 libs


```
cd yolov5_trt
mkdir build
cd build
cmake -S ..\ -B . -G  "Visual Studio 16 2019" -A x64 -DNVINFER_LIBRARY="<path/to/nvinfer.lib>" -DNVINFER_INCLUDE_DIR="<path/to/TensorRT-7.2.1.6.Windows10.x86_64.cuda-11.1.cudnn8.0\TensorRT-7.2.1.6\include"
cmake --build . --target yolov5 --config Release
cp -r Release\* ..\..\libs\ #copy over built libs to our AV model as deps
```

### Part 3 - Build the AV model

``` 
cd bigroom-models\audioModel_20211007\AVModel_VisualStudio_20211102
mkdir build
cd build
cmake -S ..\ -B . -G  "Visual Studio 16 2019" -A x64 -DNVINFER_LIBRARY="<path/to/nvinfer.lib>" -DNVINFER_INCLUDE_DIR="<path/to/TensorRT-7.2.1.6.Windows10.x86_64.cuda-11.1.cudnn8.0\TensorRT-7.2.1.6\include" -DQt5_DIR="C:\Qt\5.15.2\winrt_x64_msvc2019\lib\cmake\Qt5"
-DCMAKE_PREFIX_PATH="C:\Users\matt\Desktop\livelivelive\libtorch"
cmake --build . --target livelivelivelive_AVMv100 --config Release
```

Matt:
cmake -S ..\ -B . -G  "Visual Studio 16 2019" -A x64 -DNVINFER_LIBRARY="C:\Users\matt\Desktop\livelivelive\TensorRT-7.2.1.6\lib" -DNVINFER_INCLUDE_DIR="C:\Users\matt\OneDrive\Desktop\livelivelive\TensorRT-7.2.1.6\include" -DCMAKE_PREFIX_PATH="C:\Users\matt\Desktop\livelivelive\libtorch"

### Part 4 - Inference on a sample video

```
cd Release
.\livelivelivelive_AVMv100.exe <full/path/to/input_video.mp4> <full/path/to/input.wav> <full/path/to/output_prediction_video.mp4> <full/path/to/yolov5.wts> <full/path/to/view_size1.zip> <<full/path/to/audio_dnn_weight.pt>

