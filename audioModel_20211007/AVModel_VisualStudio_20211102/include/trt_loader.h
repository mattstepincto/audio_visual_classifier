#include <fstream>
#include <iostream>
#include <map>
#include <sstream>
#include "logging.h"
#include "NvInfer.h"
#include "cuda_runtime_api.h"
#include <opencv2/opencv.hpp>
#include <opencv2/core.hpp>
#include "yololayer.h"
#include "cuda_utils.h"
#include <chrono>


//#define USE_FP16 
#define DEVICE 0  // GPU id
#define NMS_THRESH 0.3
#define BBOX_CONF_THRESH 0.3
#define BATCH_SIZE 1


static const int INPUT_H = Yolo::INPUT_H;
static const int INPUT_W = Yolo::INPUT_W;
static const int DETECTION_SIZE = sizeof(Yolo::Detection) / sizeof(float);
static const int OUTPUT_SIZE = Yolo::MAX_OUTPUT_BBOX_COUNT * DETECTION_SIZE + 1;  // we assume the yololayer outputs no more than MAX_OUTPUT_BBOX_COUNT boxes that conf >= 0.1
static Logger gLogger;


const char* INPUT_BLOB_NAME = "data";
const char* OUTPUT_BLOB_NAME = "prob";


struct Prediction{
	std::unique_ptr<std::vector<std::vector<float>>> boxes;
	std::unique_ptr<std::vector<float>> det_scores;
	std::unique_ptr<std::vector<int>> labels;
	std::unique_ptr<std::vector<std::string>> s_ns_class;
	std::unique_ptr<std::vector<float>> det_times;
};


cv::Rect get_rect(cv::Mat& img, std::vector<float> bbox) {
	int l, r, t, b;
	float r_w = Yolo::INPUT_W / (img.cols * 1.0);
	float r_h = Yolo::INPUT_H / (img.rows * 1.0);
	if (r_h > r_w) {
		l = bbox[0] - bbox[2] / 2.f;
		r = bbox[0] + bbox[2] / 2.f;
		t = bbox[1] - bbox[3] / 2.f - (Yolo::INPUT_H - r_w * img.rows) / 2;
		b = bbox[1] + bbox[3] / 2.f - (Yolo::INPUT_H - r_w * img.rows) / 2;
		l = l / r_w;
		r = r / r_w;
		t = t / r_w;
		b = b / r_w;
	}
	else {
		l = bbox[0] - bbox[2] / 2.f - (Yolo::INPUT_W - r_h * img.cols) / 2;
		r = bbox[0] + bbox[2] / 2.f - (Yolo::INPUT_W - r_h * img.cols) / 2;
		t = bbox[1] - bbox[3] / 2.f;
		b = bbox[1] + bbox[3] / 2.f;
		l = l / r_h;
		r = r / r_h;
		t = t / r_h;
		b = b / r_h;
	}
	return cv::Rect(l, t, r - l, b - t);
}

float iou(float lbox[4], float rbox[4]) {
	float interBox[] = {
		(std::max)(lbox[0] - lbox[2] / 2.f , rbox[0] - rbox[2] / 2.f), //left
		(std::min)(lbox[0] + lbox[2] / 2.f , rbox[0] + rbox[2] / 2.f), //right
		(std::max)(lbox[1] - lbox[3] / 2.f , rbox[1] - rbox[3] / 2.f), //top
		(std::min)(lbox[1] + lbox[3] / 2.f , rbox[1] + rbox[3] / 2.f), //bottom
	};

	if (interBox[2] > interBox[3] || interBox[0] > interBox[1])
		return 0.0f;

	float interBoxS = (interBox[1] - interBox[0]) * (interBox[3] - interBox[2]);
	return interBoxS / (lbox[2] * lbox[3] + rbox[2] * rbox[3] - interBoxS);
}

bool cmp(const Yolo::Detection& a, const Yolo::Detection& b) {
	return a.conf > b.conf;
}


void nms(std::vector<Yolo::Detection>& res, float* output, float conf_thresh, float nms_thresh = 0.5) {
	int det_size = sizeof(Yolo::Detection) / sizeof(float);
	std::map<float, std::vector<Yolo::Detection>> m;

	for (int i = 0; i < output[0] && i < Yolo::MAX_OUTPUT_BBOX_COUNT; i++) {
		if (output[1 + det_size * i + 4] <= conf_thresh) continue;
		Yolo::Detection det;
		memcpy(&det, &output[1 + det_size * i], det_size * sizeof(float));
		if (m.count(det.class_id) == 0) m.emplace(det.class_id, std::vector<Yolo::Detection>());
		m[det.class_id].push_back(det);
	}
	for (auto it = m.begin(); it != m.end(); it++) {
		auto& dets = it->second;
		std::sort(dets.begin(), dets.end(), cmp);
		for (size_t m = 0; m < dets.size(); ++m) {
			auto& item = dets[m];
			res.push_back(item);
			for (size_t n = m + 1; n < dets.size(); ++n) {
				if (iou(item.bbox, dets[n].bbox) > nms_thresh) {
					dets.erase(dets.begin() + n);
					--n;
				}
			}
		}
	}
}


static inline cv::Mat preprocess_img(cv::Mat& img, int input_w, int input_h) {
	int w, h, x, y;
	float r_w = input_w / (img.cols * 1.0);
	float r_h = input_h / (img.rows * 1.0);
	if (r_h > r_w) {
		w = input_w;
		h = r_w * img.rows;
		x = 0;
		y = (input_h - h) / 2;
	}
	else {
		w = r_h * img.cols;
		h = input_h;
		x = (input_w - w) / 2;
		y = 0;
	}
	cv::Mat re(h, w, CV_8UC3);
	cv::resize(img, re, re.size(), 0, 0, cv::INTER_LINEAR);
	cv::Mat out(input_h, input_w, CV_8UC3, cv::Scalar(128, 128, 128));
	re.copyTo(out(cv::Rect(x, y, re.cols, re.rows)));
	return out;
}


class TrtLoader{
	private:
		size_t size{0};
		nvinfer1::IExecutionContext* context;
		nvinfer1::ICudaEngine* engine;
	public:
		void loadModels(std::string engine_file);
		int infer(cv::Mat image, Prediction& out_pred);
		int cuda_benchmark(static float* data, static float *prob);
		int inputIndex;
		int outputIndex;
		void* buffers[2];
		cudaStream_t stream;
		int createEngine(std::string weights_file, std::string engine_name);
};



int TrtLoader::createEngine(std::string weights_file, std::string engine_name="yolov5-large-livelivelive-stage-3cls-20210730_best.engine"){
	using namespace nvinfer1;
	IHostMemory* modelStream{nullptr};
	bool is_p6 = false;
	float gd = 1.00;
	float gw = 1.00;
	APIToModel(BATCH_SIZE, &modelStream, is_p6, gd, gw, weights_file, INPUT_BLOB_NAME, OUTPUT_BLOB_NAME);
	assert(modelStream != nullptr);
	std::ofstream p(engine_name, std::ios::binary);
	if (!p) {
		std::cerr << "could not open plan output file" << std::endl;
		return -1;
	}
	p.write(reinterpret_cast<const char*>(modelStream->data()), modelStream->size());
	modelStream->destroy();
	return 0;
}

void TrtLoader::loadModels(std::string engine_file){
	char *trtModelStream{nullptr};
	std::cout << "Loading TensorRT engine file..."<< engine_file << std::endl;
	std::ifstream file(engine_file, std::ios::binary);
	if (file.good()) {
		file.seekg(0, file.end);
		size = file.tellg();
		file.seekg(0, file.beg);
		trtModelStream = new char[size];
		assert(trtModelStream);
		file.read(trtModelStream, size);
		file.close();
		std::cout << "Engine file read successfully..." << std::endl;
	}
	else{
		std::cout << "Error reading engine filed!" << std::endl;
	}

	using namespace nvinfer1;
	IRuntime* runtime = createInferRuntime(gLogger);
    assert(runtime != nullptr);
    ICudaEngine* engine = runtime->deserializeCudaEngine(trtModelStream, size);
    assert(engine != nullptr);
    this->context = engine->createExecutionContext();
    assert(this->context != nullptr);
	assert(engine->getNbBindings() == 2);
    delete[] trtModelStream;

	this->inputIndex = engine->getBindingIndex(INPUT_BLOB_NAME);
	this->outputIndex = engine->getBindingIndex(OUTPUT_BLOB_NAME);
	assert(this->inputIndex == 0);
	assert(this->outputIndex == 1);
	std::cout << "Created the engine context successfully..." << std::endl;
	this->engine = engine;

	CUDA_CHECK(cudaMalloc(&this->buffers[this->inputIndex], BATCH_SIZE * 3 * INPUT_H * INPUT_W * sizeof(float)));
	CUDA_CHECK(cudaMalloc(&this->buffers[this->outputIndex], BATCH_SIZE * OUTPUT_SIZE * sizeof(float)));
	// Create stream
	CUDA_CHECK(cudaStreamCreate(&stream));
	//this->buffers = buffers;
	this->stream = stream;
}


int TrtLoader::infer(cv::Mat image, Prediction& pred){

	if (image.empty()) {
		std::cout << "Image is empty!" << std::endl;
		return 1;
	}

	cv::Mat pr_img = preprocess_img(image, INPUT_W, INPUT_H); // letterbox BGR to RGB
	static float data[1 * 3 * INPUT_H * INPUT_W];
	static float prob[1 * OUTPUT_SIZE];
	int i = 0;
	for (int row = 0; row < INPUT_H; ++row) {
		uchar* uc_pixel = pr_img.data + row * pr_img.step;
		for (int col = 0; col < INPUT_W; ++col) {
			data[0 * 3 * INPUT_H * INPUT_W + i] = (float)uc_pixel[2] / 255.0;
			data[0 * 3 * INPUT_H * INPUT_W + i + INPUT_H * INPUT_W] = (float)uc_pixel[1] / 255.0;
			data[0 * 3 * INPUT_H * INPUT_W + i + 2 * INPUT_H * INPUT_W] = (float)uc_pixel[0] / 255.0;
			uc_pixel += 3;
			++i;
		}
	}

	std::cout << "Image copied over...inferencing" << std::endl;
	using namespace std::chrono;
	high_resolution_clock::time_point start_time, end_time;
	if(pred.det_times != nullptr){
		start_time = high_resolution_clock::now();
	}
	nvinfer1::doInference(*(this->context), this->stream, this->buffers, data, prob, BATCH_SIZE);
	if(pred.det_times != nullptr){
		end_time = high_resolution_clock::now();
		auto time_span = duration_cast<milliseconds> (end_time - start_time);
		std::cout << "Time taken: " << time_span.count() << '\n';
		(*pred.det_times).push_back(time_span.count());
	}
	std::cout << "Yolov5 Inference done..." << std::endl;
	std::vector<std::vector<Yolo::Detection>> batch_res(1);
	auto& res = batch_res[0];
    nms(res, &prob[0 * OUTPUT_SIZE], BBOX_CONF_THRESH, NMS_THRESH);
	
	for (int k = 0; k < res.size(); k++) {
		float res_bbox[4];
		std::vector<float> coords;
		float score = res[k].conf;
		int class_id = (int)res[k].class_id;
		for (int m = 0; m < 4; m++) {
			res_bbox[m] = res[k].bbox[m];
			coords.push_back(res_bbox[m]);
		}
		
		(*pred.boxes).push_back(coords);
		(*pred.det_scores).push_back(score);

		if (class_id == 0) {
			(*pred.s_ns_class).push_back("person");
			(*pred.labels).push_back(0);
		}
		if (class_id == 1) {
			(*pred.s_ns_class).push_back("face_singing");
			(*pred.labels).push_back(1);
		}
		if (class_id == 2) {
			(*pred.s_ns_class).push_back("face_NOT_singing");
			(*pred.labels).push_back(2);
		}

	}
	std::cout << "boxes inserted: " << (*pred.boxes).size() << std::endl;
	return 0;
}



// inferencer to benchmark CUDA usage
int TrtLoader::cuda_benchmark(static float* data, static float *prob) {
	nvinfer1::doInference(*(this->context), this->stream, this->buffers, data, prob, BATCH_SIZE);
	std::cout << "Done inferencing..." << std::endl;
	std::vector<std::vector<Yolo::Detection>> batch_res(1);
	auto& res = batch_res[0];
	nms(res, &prob[0 * OUTPUT_SIZE], BBOX_CONF_THRESH, NMS_THRESH);
	return 0;
}
