#pragma once
#include <iostream>
#include <trt_loader.h>
#include <stdlib.h>



TrtLoader LoadEngineVideo(std::string engine_file, std::string wts_file, bool create_engine) {

	TrtLoader trtLoader = TrtLoader(); //Define loader 

	if (create_engine == true) {
		trtLoader.createEngine(wts_file);
		trtLoader.loadModels(engine_file);
		return trtLoader;
	}
	else {
		trtLoader.loadModels(engine_file);
		return trtLoader;
	}
}

Prediction predict_frame(TrtLoader trtLoader, cv::Mat image) {

	std::cout << "inference frame" << std::endl;

	Prediction out_pred;
	out_pred.boxes = std::unique_ptr<std::vector<std::vector<float>>>(new std::vector<std::vector<float>>());
	out_pred.det_scores = std::unique_ptr<std::vector<float>>(new std::vector<float>());
	out_pred.labels = std::unique_ptr<std::vector<int>>(new std::vector<int>());
	out_pred.s_ns_class = std::unique_ptr<std::vector<std::string>>(new std::vector<std::string>());
	out_pred.det_times = std::unique_ptr<std::vector<float>>(new std::vector<float>());

	int res = trtLoader.infer(image, out_pred);

	return out_pred;

}


