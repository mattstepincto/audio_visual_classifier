import struct
import sys
from models import *
from utils.utils import *

#@TODO: Add argumnets for cfg and weight

model = Darknet('cfg/yolov4-livelivelive.cfg', (320, 320))
weights = sys.argv[1]
device = torch_utils.select_device('0')
load_darknet_weights(model, weights)

f = open('yolov4.wts', 'w')
f.write('{}\n'.format(len(model.state_dict().keys())))
for k, v in model.state_dict().items():
    vr = v.reshape(-1).cpu().numpy()
    f.write('{} {} '.format(k, len(vr)))
    for vv in vr:
        f.write(' ')
        f.write(struct.pack('>f',float(vv)).hex())
    f.write('\n')

